//
//  UIHelper.swift
//  homework
//
//  Created by Dũng Trần on 12/6/16.
//  Copyright © 2016 Junest. All rights reserved.
//

import UIKit

typealias Completion = (() -> Void)

class UIHelper: NSObject {
    class func showAlert(withMessage message: String, inViewController controller: UIViewController, completion: Completion?) {
        let alertVC = UIAlertController(title: "SmartDev", message: message, preferredStyle: .alert)
        let actionConfirm = UIAlertAction(title: "Close", style: .default) { (alert) in
            if let nonNilCompletion = completion {
                nonNilCompletion()
            }
        }
        alertVC.addAction(actionConfirm)
        controller.present(alertVC, animated: true, completion: nil)
    }
}

extension UITextField {
    func addRadiusToConrners(corners: UIRectCorner) {
        let path = UIBezierPath(roundedRect:self.bounds,
                                byRoundingCorners:corners,
                                cornerRadii: CGSize(width: 7, height:  7))
        let maskLayer = CAShapeLayer()
        maskLayer.path = path.cgPath
        self.layer.mask = maskLayer
    }
}
